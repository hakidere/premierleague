<?php
/**
 * Created by PhpStorm.
 * User: haki
 * Date: 26.06.2015
 * Time: 20:11
 */

return array(
    'caches' => array(
        'memcache' => array(
            'adapter' => array(
                'name'     =>'memcache',
                'options'  => array(
                    'servers'   => array(
                        array(
                            '127.0.0.1',11211
                        )
                    ),
                    'namespace'  => 'premierleague',
                    'ttl' => 1800

                )
            ),
            'plugins' => array(
                'exception_handler' => array(
                    'throw_exceptions' => false
                ),
            ),
        ),
        'memcached' => array(
            'adapter' => array(
                'name'     =>'memcached',
                'options'  => array(
                    'servers'   => array(
                        array(
                            '127.0.0.1',11211
                        )
                    ),
                    'namespace'  => 'premierleague',
                    'ttl' => 1800

                )
            ),
            'plugins' => array(
                'exception_handler' => array(
                    'throw_exceptions' => false
                )
            )
        )
    )
);