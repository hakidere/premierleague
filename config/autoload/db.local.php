<?php
/**
 * Created by PhpStorm.
 * User: haki
 * Date: 26.06.2015
 * Time: 20:13
 */

Locale::setDefault('tr_TR');
putenv("NLS_LANG=TURKISH_TURKEY.AL32UTF8");
putenv('NLS_SORT=BINARY_CI');
putenv('NLS_COMP=LINGUISTIC');
return array(
    'db' => array(
        'driver' => 'Pdo',
        'dsn' => 'mysql:dbname=premier_league;host=127.0.0.1',
        'username' => 'username',
        'password' => 'password',
        'driver_options' => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''),
    )
);