<?php
/**
 * Created by PhpStorm.
 * User: mehmethakkidere
 * Date: 26.06.2015
 * Time: 14:07
 */

return array(
    'scoreTable' => array(
        'C-A-GOALS'         => array(1,1,1,2,2,3,3,3,4,4),
        'A-C-GOALS'         => array(0,1,1,1,1,2,2,2,3,3),
        'MATCH-RESULTS'     => array(1,1,0,0,0,2)
    )
);