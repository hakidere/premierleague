<?php
return array(
    'router' => array(
        'routes' => array(
            'league-home' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/premier-league-matches',
                    'defaults' => array(
                        'controller' => 'PremierLeague\Controller\Index',
                        'action'     => 'index',
                    )
                )
            ),
            'play-week' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/play-week[/:week-number]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'week-number' => '[1-6]'
                    ),
                    'defaults' => array(
                        'controller' => 'PremierLeague\Controller\Index',
                        'action' => 'playWeek',
                    ),
                ),
            ),
            'replay-league' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/replay-league',
                    'defaults' => array(
                        'controller' => 'PremierLeague\Controller\Index',
                        'action' => 'replay',
                    ),
                ),
            )
        )
    ),

    'controllers' => array(
        'invokables' => array(
            'PremierLeague\Controller\Index' => 'PremierLeague\Controller\IndexController'
        )
    ),
    'view_manager' => array(
        'template_map' => array(

        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    )
);