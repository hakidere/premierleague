<?php
/**
 * Created by PhpStorm.
 * User: haki
 * Date: 28.06.2015
 * Time: 22:15
 */

namespace PremierLeague\Controller;

use PremierLeagueCore\Controller\PremierLeagueActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class IndexController extends PremierLeagueActionController
{

    /**
     * @return array|ViewModel
     */
    public function indexAction()
    {
        $rounds = $this->generateFixture();
        $this->replayAction();

        return new ViewModel(
            array(
                'rounds' => $rounds
            )
        );
    }

    /**
     * @return JsonModel
     */
    public function playWeekAction()
    {
        $request = $this->getRequest();
        $weekNumber = $this->params()->fromRoute('week-number');

        /** @var \Zend\Cache\Storage\Adapter\Memcached $memcached */
        $memcached = $this->getCache();
        $rounds = $memcached->getItem('rounds');

        $weekPlayed = $memcached->getItem('weekPlayed');
        if (isset($weekPlayed) && $weekNumber == $weekPlayed[$weekNumber]) {
            exit;
        }
        $weekPlayed[$weekNumber] = $weekNumber;
        $memcached->setItem('weekPlayed', $weekPlayed);

        $gameScores = array();

        if ($request->isPost() && $request->isXmlHttpRequest()) {

            /** @var \PremierLeague\Model\Play $playModel */
            $playModel = $this->getServiceLocator()->get('PremierLeague\Model\Play');

            $matchedTeams = $rounds[$weekNumber - 1];
            foreach ($matchedTeams as $match) {
                $playModel->setHome($match[0]);
                $playModel->setAway($match[1]);
                $playModel->setStats();
                $playModel->playMatch();
                $gameScores[] = array(
                    'home' => $playModel->getHome(),
                    'homeScore' => $playModel->getHomeScore(),
                    'away' => $playModel->getAway(),
                    'awayScore' => $playModel->getAwayScore(),
                );
            }

            $results['table'] = $playModel->getLeagueTable();
            $results['weekNumber'] = $weekNumber;
            $results['rounds'] = $rounds;
            $results['scores'] = $gameScores;
            $results['estimations'] = $playModel->calculateEstimation();
            return $this->getLeagueTable($results);

        }
    }

    /**
     * @param array $results
     * @return JsonModel
     */
    private function getLeagueTable(array $results)
    {
        $htmlViewPart = new ViewModel();
        $htmlViewPart->setTerminal(true)
            ->setTemplate('premier-league/index/play-week')
            ->setVariables(array(
                'weekNumber' => $results['weekNumber'],
                'rounds' => $results['rounds'],
                'results' => $results
            ));

        $htmlOutput = $this->getServiceLocator()
            ->get('viewrenderer')
            ->render($htmlViewPart);

        $jsonModel = new JsonModel();
        $jsonModel->setVariables(array(
            'html' => $htmlOutput
        ));

        return $jsonModel;
    }

    public function replayAction()
    {
        /** @var \Zend\Cache\Storage\Adapter\Memcached $memcached */
        $memcached = $this->getCache();
        $memcached->removeItem('weekPlayed');
        $request = $this->getRequest();
        if ($request->isPost() && $request->isXmlHttpRequest()) {

            /** @var \PremierLeague\Model\Play $playModel */
            $playModel = $this->getServiceLocator()->get('PremierLeague\Model\Play');
            $reset = $playModel->resetLeagueTable();

            if ($reset) {
                $jsonModel = new JsonModel();
                $jsonModel->setVariables(array(
                    'reload' => '1'
                ));

                return $jsonModel;
            }
        } else {
            /** @var \PremierLeague\Model\Play $playModel */
            $playModel = $this->getServiceLocator()->get('PremierLeague\Model\Play');
            $playModel->resetLeagueTable();
        }
    }

    /**
     * Creates Fixture for League
     * @return array
     */
    public function generateFixture()
    {
        /** @var \Zend\Cache\Storage\Adapter\Memcached $memcached */
        $memcached = $this->getCache();
        $rounds = $memcached->getItem('rounds');

        if (isset($rounds)) {
            return $rounds;
        }
        $names = array('Chelsea', 'Arsenal', 'Liverpool', 'Manchester City');
        $teams = sizeof($names);

        $totalRounds = $teams - 1;
        $matchesPerRound = $teams / 2;
        $rounds = array();
        //Cyclic Algorithm Implemented
        for ($round = 0; $round < $totalRounds; $round++) {
            for ($match = 0; $match < $matchesPerRound; $match++) {
                $home = ($round + $match) % ($teams - 1);
                $away = ($teams - 1 - $match + $round) % ($teams - 1);
                if ($match == 0) {
                    $away = $teams - 1;
                }
                $rounds[$round][$match] = array($names[$home], $names[$away]);
            }
        }

        $mirrorRounds = array();
        $m = 0;
        foreach ($rounds as $mirrorRound) {
            $tmp = $mirrorRound[0][0];
            $mirrorRound[0][0] = $mirrorRound[0][1];
            $mirrorRound[0][1] = $tmp;
            $tmp = $mirrorRound[1][0];
            $mirrorRound[1][0] = $mirrorRound[1][1];
            $mirrorRound[1][1] = $tmp;
            $mirrorRounds[$m] = $mirrorRound;
            $m++;
        }

        $rounds = array_merge($rounds, $mirrorRounds);
        $keys = array_keys($rounds);
        shuffle($keys);
        $random = array();
        foreach ($keys as $key) {
            $random[] = $rounds[$key];
        }

        $memcached->setItem('rounds', $random);
        return $random;
    }
}