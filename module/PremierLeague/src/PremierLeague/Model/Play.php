<?php

/**
 * Created by PhpStorm.
 * User: mehmethakkidere
 * Date: 25.06.2015
 * Time: 20:45
 */

namespace PremierLeague\Model;

use PremierLeagueCore\Model\Db;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Sql;

class Play extends Db
{
    /**
     * Boot 1000 times to converge a realistic value
     * @var int $bootCount
     */
    protected $bootCount = 10000;
    protected $gameResult;
    protected $homeTeamGoalStats;
    protected $awayTeamGoalStats;
    protected $gameResultStats;
    protected $home;
    protected $away;
    protected $homeId;
    protected $awayId;
    protected $homeScore;
    protected $awayScore;
    protected $wonTeamId;
    protected $lostTeamId;
    protected $isDrawn;

    /**
     * @return mixed
     */
    public function getHomeScore()
    {
        return $this->homeScore;
    }

    /**
     * @return mixed
     */
    public function getAwayScore()
    {
        return $this->awayScore;
    }

    /**
     * @return array
     */
    public function getHomeTeamGoalStats()
    {
        return $this->homeTeamGoalStats;
    }

    /**
     * @param array $homeTeamGoalStats
     */
    public function setHomeTeamGoalStats($homeTeamGoalStats)
    {
        $this->homeTeamGoalStats = $homeTeamGoalStats;
    }

    /**
     * @return array
     */
    public function getAwayTeamGoalStats()
    {
        return $this->awayTeamGoalStats;
    }

    /**
     * @param array $awayTeamGoalStats
     */
    public function setAwayTeamGoalStats($awayTeamGoalStats)
    {
        $this->awayTeamGoalStats = $awayTeamGoalStats;
    }

    /**
     * @return array
     */
    public function getGameResultStats()
    {
        return $this->gameResultStats;
    }

    /**
     * @param array $gameResultStats
     */
    public function setGameResultStats($gameResultStats)
    {
        $this->gameResultStats = $gameResultStats;
    }

    /**
     * @return string
     */
    public function getHome()
    {
        return $this->home;
    }

    /**
     * @param string $home
     */
    public function setHome($home)
    {
        $this->home = $home;
    }

    /**
     * @return string
     */
    public function getAway()
    {
        return $this->away;
    }

    /**
     * @param string $away
     */
    public function setAway($away)
    {
        $this->away = $away;
    }

    /**
     * @return int
     */
    public function getHomeId()
    {
        return $this->homeId;
    }

    public function setHomeId()
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('teams');
        $select->columns(array('id'));
        $select->where(array('name' => $this->home));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute()->getResource()->fetchAll();
        $this->homeId = $results[0]['id'];
    }

    /**
     * @return int
     */
    public function getAwayId()
    {
        return $this->awayId;
    }

    public function setAwayId()
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('teams');
        $select->columns(array('id'));
        $select->where(array('name' => $this->away));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute()->getResource()->fetchAll();
        $this->awayId = $results[0]['id'];
    }

    public function setStats()
    {
        $this->setHomeId();
        $this->setAwayId();

        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('pair_stats');
        $select->columns(array('*'));
        $select->where(array('home' => $this->getHomeId(), 'away' => $this->getAwayId()));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute()->getResource()->fetchAll();
        $this->setHomeTeamGoalStats(explode(',', $results[0]['home_goals']));
        $this->setAwayTeamGoalStats(explode(',', $results[0]['away_goals']));
        $this->setGameResultStats(explode(',', $results[0]['match_results']));
    }

    public function playMatch()
    {
        $this->setHomeScore();
        $this->getMatchResult();
        // Set home score to 1 if game result is home wins and home score is 0
        if ($this->gameResult == "1" && $this->homeScore == "0") {
            $this->homeScore = "1";
            $this->awayScore = "0";
        } else {
            $this->setAwayScore();
        }

        if($this->gameResult == "1"){
            $this->wonTeamId = $this->getHomeId();
            $this->lostTeamId = $this->getAwayId();
        } elseif ($this->gameResult == "2") {
            $this->wonTeamId = $this->getAwayId();
            $this->lostTeamId = $this->getHomeId();
        }

        $this->setLeagueTable();
    }

    public function setHomeScore()
    {
        $scoreStats = $this->getHomeTeamGoalStats();
        for ($b = 0; $b < $this->bootCount; $b++) {
            $this->homeScore = $scoreStats[array_rand($scoreStats, 1)];
        }
    }

    /**
     * @return int
     */
    public function setAwayScore()
    {
        $scoreStats = $this->getAwayTeamGoalStats();
        $newScoreStats = array();
        if ($this->gameResult == "0") {
            $this->awayScore = $this->getHomeScore();
        } elseif ($this->gameResult == "1") {
            /**
             * Narrow down  $scoreStats to $newScoreStats which includes
             * items that are less than $homeScore.
             */
            for ($s = 0; $s < count($scoreStats); $s++) {
                if ($scoreStats[$s] < $this->getHomeScore()) {
                    $newScoreStats[] = $scoreStats[$s];
                }
            }
            if (!empty($newScoreStats)) {
                for ($b = 0; $b < $this->bootCount; $b++) {
                    $this->awayScore = $newScoreStats[array_rand($newScoreStats, 1)];
                }
            } else {
                $this->awayScore = $this->homeScore - 1;
            }
        } else {
            /**
             * Narrow down  $scoreStats to $newScoreStats which includes
             * items that are greater than $homeScore.
             */
            for ($s = 0; $s < count($scoreStats); $s++) {
                if ($scoreStats[$s] > $this->getHomeScore()) {
                    $newScoreStats[] = $scoreStats[$s];
                }
            }
            if (!empty($newScoreStats)) {
                for ($b = 0; $b < $this->bootCount; $b++) {
                    $this->awayScore = $newScoreStats[array_rand($newScoreStats, 1)];
                }
            } else {
                $this->awayScore = $this->homeScore + 1;
            }
        }
    }

    public function getMatchResult()
    {
        $gameResultStats = $this->getGameResultStats();
        for ($b = 0; $b < $this->bootCount; $b++) {
            $this->gameResult = $gameResultStats[array_rand($gameResultStats, 1)];
        }

        if($this->gameResult == "0"){
            $this->isDrawn = true;
        } else {
            $this->isDrawn = false;
        }
    }

    /**
     * @param $teamId
     * @return array|mixed
     */
    public function calculatePoints($teamId)
    {
        $currentTeamTable = $this->getTeamStatus($teamId);
        $calculationResults = $currentTeamTable;
        $calculationResults['p']  = $currentTeamTable['p'] +1;
        $calculationResults['ga'] = $currentTeamTable['ga']+ (int)$this->awayScore;
        $calculationResults['gf'] = $currentTeamTable['gf']+ (int)$this->homeScore;
        if($teamId == $this->getHomeId()){
            $gd = (int)($this->homeScore-$this->awayScore);
        } else {
            $gd = (int)($this->awayScore-$this->homeScore);
        }
        $calculationResults['gd'] = (int)$currentTeamTable['gd']+ $gd;
        $calculationResults['d']  = $currentTeamTable['d'];
        $calculationResults['w']  = $currentTeamTable['w'];
        $calculationResults['l']  = $currentTeamTable['l'];
        $calculationResults['pts'] = $currentTeamTable['pts'];

        if ($this->isDrawn){
            $calculationResults['d'] = $currentTeamTable['d'] + 1;
            $calculationResults['pts'] = $currentTeamTable['pts'] +1;
        } else {
            if ($this->wonTeamId == $teamId){
                $calculationResults['w'] = $currentTeamTable['w'] + 1;
                $calculationResults['pts'] = $currentTeamTable['pts'] +3;
            } else {
                $calculationResults['l'] = $currentTeamTable['l'] + 1;
            }
        }

        return $calculationResults;

    }

    /**
     * @return int $estimation
     */
    public function calculateEstimation()
    {
        $teamsStats = $this->getLeagueTable();
        $totalPoints = 0;
        foreach($teamsStats as $stat){
            $totalPoints = $totalPoints + $stat['pts'];
        }

        $estimation = array();
        foreach ($teamsStats as $stat){
            $stat['es'] = ceil(($stat['pts'] / $totalPoints) * 100);
            $estimation[] = $stat;
        }

        return $estimation;
    }


    /**
     * @return string
     */
    public function setLeagueTable()
    {
        /**
         * Database `premier_league`
         * Table `premier_league`.`premier_league_table`
         */
        $teams = array('home' => $this->homeId, 'away' => $this->awayId);
        foreach($teams as $teamId){
            $calculationResults = $this->calculatePoints($teamId);
            $sql = new Sql($this->adapter);
            $update = $sql->update('premier_league_table');
            $update->where(array('team_id' => $teamId));
            $update->set(array(
                'pts' => $calculationResults['pts'],
                'p' => $calculationResults['p'],
                'w' => $calculationResults['w'],
                'd' => $calculationResults['d'],
                'l' => $calculationResults['l'],
                'gd' =>$calculationResults['gd'],
                'gf' =>$calculationResults['gf'],
                'ga' =>$calculationResults['ga']
            ));

            $statement = $sql->prepareStatementForSqlObject($update);
            try {
                $statement->execute();
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        }
    }

    /**
     * @param $teamId
     * @return mixed
     */
    public function getTeamStatus($teamId)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('premier_league_table');
        $select->columns(array('*'));
        $select->where(array('team_id' => $teamId));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute()->getResource()->fetchAll();

        return $results[0];
    }

    public function getLeagueTable()
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array('plt' => 'premier_league_table'));
        $select->join(array('t' => 'teams'), 'plt.team_id = t.id', array('name'));
        $select->order('pts DESC');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute()->getResource()->fetchAll();

        return $results;
    }

    /**
     * @return bool|string
     */
    public function resetLeagueTable()
    {
        $sql = new Sql($this->adapter);
        $update = $sql->update('premier_league_table');
        $update->set(array(
            'pts' => '0',
            'p' => '0',
            'w' => '0',
            'd' => '0',
            'l' => '0',
            'gd' =>'0',
            'gf' =>'0',
            'ga' =>'0'
        ));

        $statement = $sql->prepareStatementForSqlObject($update);
        try {
            $statement->execute();
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return true;
    }
} 