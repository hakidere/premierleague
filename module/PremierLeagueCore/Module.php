<?php
namespace PremierLeagueCore;


use Zend\Db\Adapter\AdapterAwareInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;

class Module implements  ServiceProviderInterface
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
            ),
            'initializers' => array(
                'db' => function ($service, $sm) {
                    if ($service instanceof AdapterAwareInterface) {
                        $service->setDbAdapter($sm->get('Zend\Db\Adapter\Adapter'));
                    }
                }
            ),
            'invokables' => array(
                'PremierLeagueCore\Model\Db' => 'PremierLeagueCore\Model\Db'
            )
        );
    }
}
