<?php
/**
 * Created by PhpStorm.
 * User: haki
 * Date: 26.06.2015
 * Time: 18:41
 */

namespace PremierLeagueCore\Controller;

use Zend\Mvc\Controller\AbstractActionController;

class PremierLeagueActionController extends AbstractActionController
{

    /**
     * @return \Zend\Cache\Storage\Adapter\Memcached
     */
    public function getCache()
    {
        /** @var \Zend\Cache\Storage\Adapter\Memcached $memcached */
        $memcached = $this->getServiceLocator()->get('memcached');
        return $memcached;
    }

}