<?php
/**
 * Created by PhpStorm.
 * User: mehmethakkidere
 * Date: 26.06.2015
 * Time: 15:13
 */

namespace PremierLeagueCore\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Db\Adapter\AdapterAwareInterface;


class Db implements AdapterAwareInterface
{
    /**
     * @var Adapter
     */
    protected $adapter;

    /**
     * Set db adapter
     *
     * @param Adapter $adapter
     * @return AdapterAwareInterface
     */
    public function setDbAdapter(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }
} 