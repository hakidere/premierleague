/**
 * Created by mehmethakkidere on 26.06.2015.
 */

(function (window) {
    var currentWeekNumber = 1;
    var isPlayAll = false;

    window.currentWeekNumber = currentWeekNumber;
    window.isPlayAll = isPlayAll;

    var playWeek = function (weekNumber) {
        window.currentWeekNumber = weekNumber+1;
        $.ajax({
            url: '/play-week/' + weekNumber,
            data: {weekNumber: weekNumber},
            type: 'POST',
            success: function (data) {
                $('#play-table-display-area').append(data.html);
                $('#start-league-button').hide();
                $('#press-button-info').hide();
                if (weekNumber == 6) {
                    $('#replay-button').show();
                    $('#next-week-6').css('color', '#fff');
                    $('#next-week-6').attr('onclick', 'alreadyPlayed(' + weekNumber + ')');
                    $('.playAll').css('color', '#fff');
                    return false;
                } else {
                    $('#replay-button').hide();
                }

                if(isPlayAll == true){
                    $('#next-week-'+weekNumber).attr('onclick', 'alreadyPlayed(' + weekNumber + ')');
                    $('.nextWeek').css('color', '#fff');
                    playWeek(window.currentWeekNumber);
                }
            }
        });
    }
    window.playWeek = playWeek;

    var disableClick = function (e, weekNumber) {
        e.attr('onclick', 'alreadyPlayed(' + weekNumber + ')');
        e.css('color', '#fff');
    }

    window.disableClick = disableClick;

    var alreadyPlayed = function (weekNumber) {
        alert('Week ' + weekNumber + ' Already played!');
    }
    window.alreadyPlayed = alreadyPlayed;


    var replayLeague = function (weekNumber) {
        $.ajax({
            url: '/replay-league',
            type: 'POST',
            success: function (data) {
                if (data.reload == "1") {
                    window.location.reload();
                }
            }
        });
    }
    window.replayLeague = replayLeague;

    var playAll = function () {
        isPlayAll =true;
        playWeek(window.currentWeekNumber);
    }

    window.playAll = playAll;

})(window);

